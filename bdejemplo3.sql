﻿-- creación de la base de datos de la hoja 1 de ejercicios, ejercicio 3
DROP DATABASE IF EXISTS bdejemplo3;
CREATE DATABASE bdejemplo3;
USE bdejemplo3;

-- creamos las tablas con sus claves 
CREATE TABLE profesores(
  dni varchar(9),
  nombre varchar(20),
  direccion varchar(20), 
  telefono varchar(9),
  PRIMARY KEY (dni)
  );

CREATE TABLE modulos(
  codigo varchar(10),
  nombre varchar(20),
  PRIMARY KEY (codigo)
  );


CREATE TABLE alumnos(
  numexp varchar(10),
  nombre varchar(20),
  apellidos varchar(20),
  fecha_nac date,
  PRIMARY KEY (numexp)
  );

CREATE TABLE esDelegado(
  expDelegado varchar(10),
  expAlumno  varchar(10),
  PRIMARY KEY (expDelegado, expAlumno),
  UNIQUE KEY (expAlumno),
  CONSTRAINT fkesdelegadodelegado FOREIGN KEY (expDelegado) REFERENCES alumnos (numexp),
  CONSTRAINT fkesdelegadoalumno FOREIGN KEY (expAlumno) REFERENCES alumnos (numexp)
  );

CREATE TABLE imparten(
  dniProfesor varchar(9),
  codigoModulo varchar(10),
  PRIMARY KEY (dniProfesor, codigoModulo),
  UNIQUE KEY (codigoModulo),
  CONSTRAINT fkimpartenprofesor FOREIGN KEY (dniProfesor) REFERENCES profesores (dni),
  CONSTRAINT fkimpartenmodulo FOREIGN KEY (codigoModulo) REFERENCES modulos (codigo)
  );

CREATE TABLE cursa(
  codigoModulo  varchar(10),
  expAlumno   varchar(10),
  PRIMARY KEY (codigoModulo, expAlumno),
  CONSTRAINT fkcursamodulo FOREIGN KEY (codigoModulo) REFERENCES  modulos (codigo),
  CONSTRAINT fkcursaalumno FOREIGN KEY (expAlumno) REFERENCES alumnos (numexp)
  );


-- insertamos datos en las tablas
  INSERT INTO profesores (dni, nombre, direccion, telefono)
  VALUES ('123456789', 'luis', 'calle1', '999999999');

  INSERT INTO modulos (codigo, nombre)
  VALUES ('mod01', 'bases de datos');

  INSERT INTO alumnos (numexp, nombre, apellidos, fecha_nac)
  VALUES ('exp01', 'pepe', 'ape1', '01/01/2000'),
  ('exp02', 'manuel', 'ape2', '02/01/2000'),
  ('exp03', 'ana', 'ape3', '03/01/2000');

  INSERT INTO esDelegado (expDelegado, expAlumno)
  VALUES ('exp01', 'exp02'),
  ('exp01','exp03');

  INSERT INTO imparten (dniProfesor, codigoModulo)
  VALUES ('123456789', 'mod01');

  INSERT INTO cursa (codigoModulo, expAlumno)
  VALUES ('mod01', 'exp01'),
  ('mod01','exp02'),
  ('mod01','exp03');


  -- error porque exp03 solo puede tener un delegado, es unique key por enunciado
  /*
  INSERT INTO esDelegado (expDelegado, expAlumno)
  VALUES ('exp02', 'exp03');
  */

  